# README #

Hey friends! This is the source code of our robot.

### Help! I must contact someone about this code! ###

* Alex Taber (aft.pokemon@gmail.com)
* Someone on our [site](http://first.raidtech.net)

### License ###
This project is licensed under the terms of the MIT license.